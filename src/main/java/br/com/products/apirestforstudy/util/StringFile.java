package br.com.products.apirestforstudy.util;

public interface StringFile {
	
	//Entity Product
	String PRODUCT_DESCRIPTION_MESSAGE_VALIDATION = "Please provide a description";
	
	String PRODUCT_STOCK_MESSAGE_VALIDATION = "Please provide a stock";
	
	String PRODUCT_PRICE_MESSAGE_VALIDATION = "Please provide a price";
	
	String PRODUCT_IMAGE_URL_MESSAGE_VALIDATION = "Please provide a image url";

	
	//Product Resources (API DOCUMENTATION)
	String PRODUCT_RESOURCE_API_DOC_GET_FIND_ALL = "Returns all saved products";
	
	String PRODUCT_RESOURCE_API_DOC_GET_BY_ID = "Returns the product by its identifier";
	
	String PRODUCT_RESOURCE_API_DOC_POST_SAVE_ONE = "Save one product in database";
	
	String PRODUCT_RESOURCE_API_DOC_POST_SAVE_ALL = "Save product list in database";

	String PRODUCT_RESOURCE_API_DOC_PUT_UPDATE_BY_OBJECT = "Update product in database by product object";

	String PRODUCT_RESOURCE_API_DOC_DELETE_BY_ID = "Delete product in database by indentifier";
	
	String PRODUCT_RESOURCE_API_DOC_DELETE_BY_OBJECT = "Delete product in database by product object";
}
