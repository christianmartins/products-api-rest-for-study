package br.com.products.apirestforstudy.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.products.apirestforstudy.data.repository.ProductRepository;
import br.com.products.apirestforstudy.domain.model.Product;
import br.com.products.apirestforstudy.util.StringFile;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/product")
@Api(value = "API REST products")
@CrossOrigin(origins = "*")
public class ProductResource {
	@Autowired
	ProductRepository productRepository;
	
	@GetMapping("/findAll")
	@ApiOperation(value = StringFile.PRODUCT_RESOURCE_API_DOC_GET_FIND_ALL)
	public List<Product> findAll(){
		return productRepository.findAll();
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value = StringFile.PRODUCT_RESOURCE_API_DOC_GET_BY_ID)
	public Product findById(@PathVariable(value = "id") long id){
		return productRepository.findById(id);
	}
	
	@PostMapping()
	@ApiOperation(value = StringFile.PRODUCT_RESOURCE_API_DOC_POST_SAVE_ONE)
	public Product save(@Valid @RequestBody Product product){
		return productRepository.save(product);
	}
	
	@PostMapping("saveAll")
	@ApiOperation(value = StringFile.PRODUCT_RESOURCE_API_DOC_POST_SAVE_ALL)
	public List<Product> save(@Valid @RequestBody List<Product> products){
		return productRepository.saveAll(products);
	}
			
	@PutMapping()
	@ApiOperation(value = StringFile.PRODUCT_RESOURCE_API_DOC_PUT_UPDATE_BY_OBJECT)
	public Product update(@Valid @RequestBody Product product) {
		return productRepository.save(product);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = StringFile.PRODUCT_RESOURCE_API_DOC_DELETE_BY_ID)
	public void deleteById(@PathVariable(value = "id") long id) {
		productRepository.deleteById(id);
	}
	
	@DeleteMapping()
	@ApiOperation(value = StringFile.PRODUCT_RESOURCE_API_DOC_DELETE_BY_OBJECT)
	public void delete(@Valid @RequestBody Product product) {
		productRepository.delete(product);
	}
}
