package br.com.products.apirestforstudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApirestforstudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApirestforstudyApplication.class, args);
	}

}
