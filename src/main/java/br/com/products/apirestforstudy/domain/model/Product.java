package br.com.products.apirestforstudy.domain.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import br.com.products.apirestforstudy.util.StringFile;

@Entity
@Table(name="TB_PRODUCT")
public class Product implements Serializable  {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

    @NotNull(message = StringFile.PRODUCT_DESCRIPTION_MESSAGE_VALIDATION)
 	private String description;
    
    @NotNull(message = StringFile.PRODUCT_STOCK_MESSAGE_VALIDATION)
    @DecimalMin("0.0")
	private BigDecimal stock;
    
    @NotNull(message = StringFile.PRODUCT_PRICE_MESSAGE_VALIDATION)
    @DecimalMin("0.0")
	private BigDecimal price;
    
    @NotNull(message = StringFile.PRODUCT_IMAGE_URL_MESSAGE_VALIDATION)
	private String imageUrl;
    	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public BigDecimal getStock() {
		return stock;
	}
	public void setStock(BigDecimal stock) {
		this.stock = stock;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}	
}
