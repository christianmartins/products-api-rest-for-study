package br.com.products.apirestforstudy.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.products.apirestforstudy.domain.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
	Product findById(long id);
}
